# aws-policy

This is SCP automantion pipeline. The pipeline will handle listed actions for SCPs: create, 
delete, modify, associate (attach) with an entity(target), import existing policy and manage imported policy.

## Pre-requisite

- User with permissions to create/modify and apply SCPs in any entity (OU, Account) – PROD jobs (PROD user - role with permission to attach/detach SCP to any entity)
- User with permissions to create/modify SCP but permission limited to apply SCP only in test entities (OU and Account) – Test jobs (Test User – role with restricted permission to attach /detach SCP)
- GitLab Repository
- [Terraform](https://www.terraform.io) 
- AWS credentials

## Design
- See [Design Document](https://globe.upc.biz/confluence/display/DCTC/Deployment+pipeline+for+AWS+SCP)

## How to run the CI/CD pipeline

We have 2 pipelines that will manage deployment for all SCPs. one pieline to manage test deployment and another for prod deployment.
Test pipeline to be invoked manually after pull request files is approved.Prod pipeline to be invoked manually after merging the feature branch to master.

## Repository

- aws-scp folder will hold all the json files containing policies.
- tfscripts will contain all terraform files as described below. 
  - resource.tf file contains infrastructure code. 
  - backend.tf file is used to store state information on s3 location in Shared Services account.
  - variable.tf contains all the variables to be used in resource.tf file.
  - provider.tf will contains provider details.
- .ci-gitlab.yml configuration file contains the jobs to be run using docker executor from the docker image with all necessary services that are needed to execute our pipeline.

## Pipeline

### Local System 

- Visual Studio with HashiCorp Terraform v2.11.0 extension
- Configure Gitlab repo in Visual Studio

### Gitflow branching Strategy 

Create a feature branch from dev branch. Commit all changes to feature branch, once the result looks good raise merge request to dev branch which in turn is verified and merged to master. 

### Trigger pipeline

Policy files or code changes are commited and pushed to feature branch. Once new commit is done test pipeline will start
executing. We will verify the changes applied and merge request will be raised if result looks good. After merging code to master prod
pipeline will start executing.

### Stages in pipeline

- terraform init - It is used to initialize a working directory containing Terraform configuration files. 
- terraform validate - Validate runs checks that verify whether a configuration is syntactically valid and internally consistent, regardless of any provided variables or existing state.
- terraform import aws_organizations_policy.policy-demo-import $POLICY_ID - It is used to import existing resources into Terraform. 
- terraform plan -out "planfile" - The terraform plan command creates an execution plan. By default, creating a plan consists of:
  - Reading the current state of any already-existing remote objects to make sure that the Terraform state is up-to-date.
  - Comparing the current configuration to the prior state and noting any differences.
  - Proposing a set of change actions that should, if applied, make the remote objects match the configuration. 
  - -out option usedt to save the generated plan to a file, which can be later executed by passing the file to terraform apply as an extra argument.
- terraform apply -input=false "planfile" - This command executes the actions proposed in a Terraform plan provided as argument here. The option -input=false disables all of Terraform's interactive prompts. 
  
## Outcome 

The policy being created in AWS Service Control Policy. It will be attached to OU or Account based on input provided.
Importing existing policy and managing imported policy will be one time activity. Later the import stage will be removed for SCP creation and modification.   
  
## Future Enhancement
    
-  [HashiCorp Vault](https://www.vaultproject.io/docs/what-is-vault) integration.
-  Introduction of automated testing.
-  yaml file to be used for policy to entity mapping. 

## jira ticket number 	DCCR-822
