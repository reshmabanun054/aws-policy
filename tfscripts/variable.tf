variable "target_id" {
  default = "ou-z10r-t3g3v3tk"
  type = string
}

variable "file_path" {
  default = "/builds/reshmabanun054/aws-policy/aws-scp/OU/policy-demo.json"
  type = string
}
