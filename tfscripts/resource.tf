resource "aws_organizations_policy" "demopolicy" {
  name ="demopolicy"
  content = jsonencode(jsondecode(file(var.file_path)))
}

resource "aws_organizations_policy_attachment" "demopolicy_attach" {
  policy_id = aws_organizations_policy.demopolicy.id
  target_id = var.target_id
}
